<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseSuccess($data, $message = 'success') {
        return response()->json([
            'message' => 'success',
            'result' => $data,
            
        ], 200, [
            'Content-type' => 'application/json; charset=utf-8',
            'Charset' => 'utf-8'
        ], JSON_UNESCAPED_UNICODE);

    }
}
